from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import MataKuliah
from .forms import FormulirMatkul

# Create your views here.
def index(request) :
    kumpulan_matkul = MataKuliah.objects.all()
    response = {"kumpulan_matkul" : kumpulan_matkul}
    return render(request, 'story-5.html', response)

def isiMatkul(request):
    form = FormulirMatkul()
    return render(request, 'isi-matkul.html', {'form' : form})

def detailMatkul(request, matkul_id):
    detail_matkul = MataKuliah.objects.get(id=matkul_id)
    response = {"detail_matkul" : detail_matkul}
    return render(request, 'detail-matkul.html', response)

def tambahMatkul(request):
    if request.method == 'POST' :
        form = FormulirMatkul(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/story5/')
            
def hapusMatkul(request, matkul_id):
    matkul_dihapus = MataKuliah.objects.get(id=matkul_id)
    matkul_dihapus.delete()
    return HttpResponseRedirect('/story5/')